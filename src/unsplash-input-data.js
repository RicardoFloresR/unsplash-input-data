import {LitElement, html, css} from 'lit-element';

export class UnsplashInputData extends LitElement {
	static get is() {
		return 'unsplash-input-data';
	}

	static get properties() {
		return{};
	}

	static get styles() {
		return css`
		#main-container{
			width: 100%;
			display: flex;
			flex-direction: row;
		}
		#input{
			width: 80%;
			font-size: 1rem;
			boder-radius: 5px;
			padding: 0.3rem;
			padding-rigth: 0.5rem;
			padding-left: 0.5rem;
		}

		#button-data{
			width: 20%;
			font-size: 1rem;
			color: white;
			background-color: #072146;
			border-top-right-radius: 5px;
			border-bottom-right-radius: 5px;
		}

		#button-data:hover{
			background-color: #16335c;
		}
		`;
	}

	constructor() {
		super();
	}

	render() {
		return html`
		<div id="main-container">
			<input type="text" id="input">
			<button 
			id="button-data"
			@click="${this.sendQuery}">Buscar</button>
		</div>
		`;
	}

	sendQuery(e) {
		const query = this.shadowRoot.getElementById('input').value.toLowerCase().replace(/ /g, '_');
		this.dispatchEvent(new CustomEvent('unsplash-input-send-sata', {
			bubbles: true,
			composed: true,
			detail: query
		}));
	}
}

customElements.define(UnsplashInputData.is, UnsplashInputData);